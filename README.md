# ntpshm conda recipe

Home: "https://gitlab.esss.lu.se/johnsparger/ntpshm"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: An EPICS module to expose the contents of the NTP shared memory segment as PVs
